import os
from data import data
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from dotenv import load_dotenv
load_dotenv()

#config pour le serveur smtp
config_email = os.environ.get("EMAIL")
config_password = os.environ.get("PASSWORD")
config_server = os.environ.get("SERVER_SMTP")
config_server_port = os.environ.get("PORT_SMTP")

telephon_number = os.environ.get("TELEPHON_NUMBER")
attachment_path = os.environ.get("ATTACHMENT_PATH")

def createContent(sender:str, recipient:str, subject:str, content:str, name:str):
    message = MIMEMultipart('alternative')
    message['Subject'] = subject
    message['From'] = sender
    message['To'] = recipient
    html = f"""\
    <html>
        <boby>
            <p>Cher directeur de l'agence {name},</p>
            <p>
            J'espère que vous allez bien. Je me permets de vous contacter car je suis
            vivement intéressé par l'opportunité de contribuer en tant que développeur
            au sein de votre agence de communication, même si aucune annonce n'a été
            publiée.
            </p>
            <p>
            Actuellement étudiant en troisième année de licence en informatique avec
            une spécialisation en cybersécurité, je suis passionné par les enjeux de
            sécurité informatique et je suis convaincu que ces compétences peuvent
            être mises au service de votre agence.
            </p>
            <p>
            Mon curriculum vitae en pièce jointe détaille mon parcours académique, mes
            compétences techniques en développement informatique, ainsi que ma
            formation en cybersécurité. J'ai développé des compétences en conception,
            en programmation sécurisée, et en gestion de la sécurité des systèmes.
            </p>
            <p>
            Je suis persuadé que votre agence pourrait bénéficier de mon expertise en
            développement informatique, avec un fort accent sur la cybersécurité. Je
            suis ouvert à discuter des modalités de l'alternance, y compris le
            déroulement de 3 semaines en entreprise et une semaine en école, conforme
            à mon programme académique.
            </p>
            <p>
            Je serais ravi de vous rencontrer pour discuter de la possibilité de
            travailler ensemble. Vous pouvez me contacter par e-mail à
            <a href="mailto:{sender}">{sender}</a> ou par téléphone au
            <a href="tel:{telephon_number}">{telephon_number}</a>.
            </p>
            <p>
            Je vous remercie pour votre considération, et j'espère avoir l'opportunité
            de discuter davantage de ma candidature.
            </p>
            <p>Cordialement,<br>
                Kévin CALMO</p>
        </boby>
    </html>
    """
    
    send_message = MIMEText(html,"html")
    message.attach(send_message)
    
    
    try:
        with open(attachment_path,"rb") as attachment:
            p = MIMEApplication(attachment.read(), _subtype="pdf")
            p.add_header('Content-Disposition', "attachment;filename= %s" % attachment_path.split("./assets/")[-1])
            message.attach(p)
    except Exception as e:
        print(str(e))
        
    return message.as_string()



def sendEmail(recipient_email:str, message:str):
    server_mail = smtplib.SMTP(config_server,config_server_port)
    server_mail.starttls()
    server_mail.login(config_email,config_password)
    server_mail.sendmail(config_email,recipient_email,message)
    server_mail.quit()
    print(f"\nEmail envoyée avec succès à {recipient_email}")

def sendMultipleEmail(list):
    for item in data:
        if item['EMAIL'] != "" :
            email = item['EMAIL']
            content = createContent(
                config_email,
                email,
                "Demande alternance d'Octobre 2023 à Septembre 2024",
                "mon message",
                item['NOM']
                )
            sendEmail(email,content)
            



sendEmail(
    "kevinfcalmo@gmail.com",
    createContent(config_email,"kevinfcalmo@gmail.com","Demande alternance d'Octobre 2023 à Septembre 2024","","Fake Agency")
    )

